import React from 'react';
import {Col} from "reactstrap";
import Country from "./Country/Country";
import './CountriesList.css'

const CountriesList = ({countries,countryChoice}) => {
    return (
        <Col  md="4" lg="3" className="list">
            <ul>
                {countries.map(function (country) {
                    return <Country key={country.alpha3Code} country={country} onClick={countryChoice} />
                })}
            </ul>
        </Col>
    );
};

export default CountriesList;