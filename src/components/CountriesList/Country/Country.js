import React, {Component} from 'react';

class Country extends Component {
    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return false;
    }

    render() {
        return (
            <div>
                <li style={{"listStyle": "none", cursor:"pointer"}} onClick={() => this.props.onClick(this.props.country.alpha3Code)}>
                    {this.props.country.name}
                </li>
            </div>
        );
    }
}

export default Country;
