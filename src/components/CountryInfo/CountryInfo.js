import React, {Component} from 'react';
import {Col, Row} from "reactstrap";
import axios from "axios";


class CountryInfo extends Component {
    state = {
        loadedCountry: null,
        borderCountriesNames: []
    };

    async componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.alpha3Code && this.props.alpha3Code !== prevProps.alpha3Code) {
            const response = await axios.get("https://restcountries.eu/rest/v2/alpha/" + this.props.alpha3Code);
            const loadedCountry = response.data;
            const borderCountriesNames = await Promise.all(loadedCountry.borders.map(async function (border) {
                const borderCountry = await axios.get("https://restcountries.eu/rest/v2/alpha/" + border);
                return borderCountry.data.name;
            }));
            this.setState({loadedCountry, borderCountriesNames });
        }
    }

    render() {
        return this.state.loadedCountry?(
            <Col md="8" lg="9">
                <Row>
                    <Col sm="6">
                        <div className="fullCountry">
                            <h1>
                                {this.state.loadedCountry.name}
                            </h1>
                            <p><b>Capital: </b>{this.state.loadedCountry.capital}</p>
                            <p><b>Population: </b>{this.state.loadedCountry.population}</p>
                        </div>
                    </Col>
                    <Col sm="6">
                        <img style={{width:"150px", border:"1px solid black"}} src={this.state.loadedCountry.flag} alt=""/>
                    </Col>
                </Row>
                <p><b>Borders with: </b></p>
                <ul>
                    {this.state.borderCountriesNames.map(function (name) {
                        return <li key={name}>{name}</li>;
                    })}
                </ul>
            </Col>
        ): <p>Сhoose a country</p>;
    }
}

export default CountryInfo;
