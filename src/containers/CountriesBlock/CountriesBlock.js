import React, {Component} from 'react';
import {Container, Row} from "reactstrap";
import CountriesList from "../../components/CountriesList/CountriesList";
import CountryInfo from "../../components/CountryInfo/CountryInfo";
import axios from "axios";
class CountriesBlock extends Component {
    state = {
        countries: [],
        selectedCountryCode: ""
    };
    countryChoice = (code) =>{
        this.setState({selectedCountryCode:code});
    };
    async componentDidMount() {
        const response = await axios.get("https://restcountries.eu/rest/v2/all?fields=name;alpha3Code");
        const countries = response.data;
        this.setState({countries});
    }

    render() {
        return (
            <Container style={{marginTop:"10px"}}>
                <Row>
                    <CountriesList countries={this.state.countries} countryChoice={this.countryChoice}/>
                    <CountryInfo  alpha3Code={this.state.selectedCountryCode}/>
                </Row>
            </Container>
        );
    }
}

export default CountriesBlock;