import React from 'react';
import CountriesBlock from "./containers/CountriesBlock/CountriesBlock";

function App() {
    return (
        <div className="App">
            <CountriesBlock/>
        </div>
    );
}

export default App;
